module.exports = app => {
    const users = require('../controllers/users.controllers') // --> ADDED THIS

    // Iniciar Sesion 
    app.post("/sessions", users.session);
    app.get("/users/:userId", users.findOne);
    app.get("/users.accesos.carpetas", users.accesosCarpetas);


};