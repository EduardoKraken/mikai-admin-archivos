// IMPORTAR DEPENDENCIAS
const express    = require("express");
const bodyParser = require("body-parser");
var   cors       = require('cors');

const fileUpload = require("express-fileupload");

// IMPORTAR EXPRESS
const app = express();

//Para servidor con ssl
var http    = require('http');
const https = require('https');
const fs    = require('fs');

// IMPORTAR PERMISOS
app.use(cors());
// parse requests of content-type: application/json
app.use(bodyParser.json());
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.use(fileUpload()); //subir archivos


app.use("/archivos.mikai",            express.static("./../../archivos-mikai"));


// ----IMPORTAR RUTAS---------------------------------------->
require('./routes/users.routes')( app );
require('./routes/formatos.routes')( app );
require('./routes/carpetas.routes')( app );

// ----FIN-DE-LAS-RUTAS-------------------------------------->

// DEFINIT PUERTO EN EL QUE SE ESCUCHARA
app.listen(3006, () => {
  console.log("|****************** S-O-F-S-O-L-U-T-I-O-N *********************| ");
  console.log("|******************** C-R-E-A-T-I-B-E *************************| ");
  console.log("|**************************************************************| ");
  console.log("|************ Servidor Corriendo en el Puerto 3006 ************| ");
});


//Para servidor con ssl

// https.createServer({
//     key: fs.readFileSync('/etc/letsencrypt/live/support-smipack.com/privkey.pem'),
//     cert: fs.readFileSync('/etc/letsencrypt/live/support-smipack.com/fullchain.pem'),
//     passphrase: 'desarrollo'
// }, app)
// .listen(3006);
