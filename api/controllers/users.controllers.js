const { countBy } = require("lodash");
const Users = require("../models/users.model.js");

exports.session = (req, res) => {
    Users.session(req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Este cliente no se encuentra registrado`
                });
            } else {
                res.status(500).send({
                    message: "Error al buscar el usuario"
                });
            }
        } else
            console.log('data', data)
        res.send(data);
    });
};

// Find a single users with a usersId
exports.findOne = (req, res) => {
    Users.findOne(req.params.userId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `No encontre el cliente con el id ${ req.params.userId }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al recuperar el usuario con el id" + req.params.userId
                });
            }
        } else res.send(data);
    });
};

exports.accesosCarpetas = async(req, res) => {
    try {
        let accesos = [];
        let usuarios = await Users.consultaUsuariosActivos().then(response => response);

        for (let item of usuarios) {
            let accesos_usuario = await Users.accesosCarpetas(item.id).then(response => response);
            accesos.push({ usuario: item, accesos: accesos_usuario })
        }
        return res.send(accesos)

    } catch (err) {
        res.status(500).send({
            message: err.message || "Se produjo algún error al consultar accesos a carpetas"
        })
    }
};