const sql = require("./db.js");

// constructor
const Users = function(user) {
    this.nombre = user.nombre;
    this.apellidos = user.apellidos;
    this.empresa = user.empresa;
    this.telempresa = user.telempresa;
    this.telmovil = user.telmovil;
    this.email = user.email;
    this.modmaquina = user.modmaquina;
    this.matserie = user.matserie;
    this.password = user.password;
    this.estatus = user.estatus;
};

// VARIABLES PARA QUERYS

Users.session = (loger, result) => {
    sql.query("SELECT * FROM usuarios WHERE email = ? AND password = ?", [loger.email, loger.password], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return
        }
        console.log('res', res)
        result(null, res[0])
    });
};

Users.findOne = (userid, result) => {
    sql.query(`SELECT * FROM usuarios WHERE id = ${userid}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found user: ", res[0]);
            result(null, res[0]);
            return;
        }
        // not found Customer with the id
        result({ kind: "not_found" }, null);
    });
};

Users.accesosCarpetas = (id_usuarios) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT
                    a.*
                    ,c.carpeta AS nombre_carpeta
                FROM mikai.accesos_usuarios_carpetas AS a 
                  JOIN admincarpetas.carpetas AS c ON a.id_carpetas = c.idcarpetas
                WHERE 
                    a.estatus = 1 
                AND a.deleted = 0
                AND a.id_usuarios = ?`, [id_usuarios], (err, res) => {
            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve(res);
        });
    })
};

Users.consultaUsuariosActivos = () => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT
                      u.id
                      ,u.nomuser AS nombre_usuario
                      ,u.admin AS nivel_usuario
                    FROM mikai.usuarios AS u
                  WHERE 
                    u.estatus = 1`, (err, res) => {
            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve(res);
        });
    })
};




module.exports = Users;