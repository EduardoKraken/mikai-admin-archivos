import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import rutas from '@/router'

import Login                 from '@/views/Login.vue'
import Home                  from '@/views/Home.vue'
import AdministrarArchivos   from '@/views/AdministrarArchivos.vue'
import AdministrarAccesos    from '@/views/AdministrarAccesos.vue'
import Carpetas              from '@/views/Carpetas.vue'
import Archivos              from '@/views/Archivos.vue'

// Reportes


Vue.use(VueRouter)

// const routes: 

const router = new VueRouter({
  mode: '',
  base: process.env.BASE_URL,
  routes: [

    //MODULO DE LOGIN
    { path: '/login/:id'           , name: 'login'               , component: Login, 
      meta: { libre: true}},
    
    { path: '/home'                , name: 'home'                , component: Home, 
      meta: { ADMIN: true, USUARIO: true, SUPERVISOR: true }},
    
    { path: '/administrararchivos' , name: 'AdministrarArchivos' , component: AdministrarArchivos, 
      meta: { ADMIN: true, SUPERVISOR: true }},
    
    { path: '/administraraccesos'  , name: 'AdministrarAccesos'  , component: AdministrarAccesos, 
      meta: { ADMIN: true, SUPERVISOR: true }},
    
    { path: '/carpetas'            , name: 'Carpetas'            , component: Carpetas, 
      meta: { ADMIN: true, USUARIO: true, SUPERVISOR: true }},
    
    { path: '/archivos'            , name: 'Archivos'            , component: Archivos, 
      meta: { ADMIN: true, SUPERVISOR: true }},
  ]
})

router.beforeEach( (to, from, next) => {
  // console.log('Entro',store.state.Login.getusuarioAdminArchivos)

  if(to.matched.some(record => record.meta.libre)){
    next()
  }else if(store.state.Login.usuarioAdminArchivos.admin === 'ADMIN'){
    if(to.matched.some(record => record.meta.ADMIN)){
      next()
    }
  }else if(store.state.Login.usuarioAdminArchivos.admin === 'USUARIO'){
    if(to.matched.some(record => record.meta.USUARIO)){
      next()
    }
  }else if(store.state.Login.usuarioAdminArchivos.admin === 'SUPERVISOR'){
    if(to.matched.some(record => record.meta.SUPERVISOR)){
      next()
    }
  }else{
    next({
      name: 'login'
    })
  }
})

export default router
