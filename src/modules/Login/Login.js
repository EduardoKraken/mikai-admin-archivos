import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'
import vuetify from '@/plugins/vuetify';

export default{
	namespaced: true,
	state:{
		loginAdminArchivos:false,
		usuarioAdminArchivos:'',
	},

	mutations:{
		LOGEADO(state, value){
			state.loginAdminArchivos = value
		},
		DATOS_USUARIO(state, usuarioAdminArchivos){
      state.usuarioAdminArchivos = usuarioAdminArchivos
		},

		SALIR(state){
			state.loginAdminArchivos = false
			state.usuarioAdminArchivos = ''
		}
	},

	actions:{
		// Valida si el usario existe en la BD
		validarUser({commit}, usuario){
			return new Promise((resolve, reject) => {
			 // console.log (usuario)
			  Vue.http.post('sessions', usuario).then(respuesta=>{
			  	return respuesta.json()
			  }).then(respuestaJson=>{
	         // console.log('respuestaJson',respuestaJson)
					if(respuestaJson == null){
						resolve(false) 

					}else{
						resolve(respuestaJson) 
        	}
      	}, error => {
        	reject(error)
      	})
			})
		},

		GetInfoUser({commit, dispatch}, usuario){
			return new Promise((resolve, reject) => {
			  Vue.http.post('sessions', usuario).then(response=>{
			  	console.log( response.body )
					if(!response.body){
						resolve(false) 
					}else{
        		commit('DATOS_USUARIO',response.body)
						commit('LOGEADO', true)
						resolve(true)
	      	}
	    	}, error => {
	      	resolve(false)
	    	})
			})
		},

		salirLogin({commit}){
			commit('SALIR')
		},
	},

	getters:{
		logeadoAdminArchivos(state){
		  return state.loginAdminArchivos
		},
		getusuarioAdminArchivos(state){
			return state.usuarioAdminArchivos
		},

	}
}